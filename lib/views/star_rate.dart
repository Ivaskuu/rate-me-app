import 'package:flutter/material.dart';
import '../misc/mycolors.dart';
import 'dart:async';

/// A row of star-form icons
/// 
/// This is a test to see if the dartdoc works.
class ViewStarRate extends StatefulWidget
{
  final double rate;
  final String objId;
  final double starSize;
  final bool whiteColor;
  Function execOnRated;

  ViewStarRate({this.rate, this.objId, this.starSize = 60.0, this.whiteColor = false, this.execOnRated});

  @override
  _ViewStarRateState createState() => new _ViewStarRateState();
}

class _ViewStarRateState extends State <ViewStarRate>
{
  /// Used to calculate the number of stars to light up when evaluating.
  int stars = 0;

  /// It is defined when the user starts evaluating to determine if he will give a high or low rate. Used to improve the evaluating experience.
  /// 
  /// If the user starts evalutaing from the right part of the screen, it is often because
  /// he wants to give a low evaluation, and vice-versa.
  bool direction;

  /// The screen X position from where the user starts swiping.
  double startDragPosX;

  /// The distance between a star becomes added or removed to the evalutation.
  final double starDragDist = 28.0;

  @override
  Widget build(BuildContext context)
  {
    if(widget.rate != null)
    {
      return new Row
      (
        mainAxisAlignment: MainAxisAlignment.center,
        children: getStars(widget.rate),
      );
    }
    else
    return new GestureDetector
    (
      onHorizontalDragStart: (DragStartDetails details)
      {
        startDragPosX = details.globalPosition.dx;

        if(startDragPosX < 165)
        {
          stars = 1;
          direction = true; // Going for a positive valutation
        }
        else
        {
          stars = 5;
          direction = false; // Going for a negative valutation
        }
        
        setState(() => null);
      },
      onHorizontalDragUpdate: (DragUpdateDetails details)
      {
        // Choosing the valutaion mode (positive, true, or negative, false)
        int newStars;
        if(direction) newStars = (details.globalPosition.dx - startDragPosX) ~/ starDragDist;
        else newStars = 5 - (startDragPosX - details.globalPosition.dx) ~/ starDragDist;

        if(newStars != stars && newStars >= 1 && newStars <= 5)
        {
          setState(() => stars = newStars);
        }
      },
      onHorizontalDragEnd: (_)
      {
        // TODO: Send the rating
        widget.execOnRated(stars);
      },
      child: new Row
      (
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>
        [
          new StarIcon(stars >= 1, widget.starSize, widget.whiteColor),
          new StarIcon(stars >= 2, widget.starSize, widget.whiteColor),
          new StarIcon(stars >= 3, widget.starSize, widget.whiteColor),
          new StarIcon(stars >= 4, widget.starSize, widget.whiteColor),
          new StarIcon(stars >= 5, widget.starSize, widget.whiteColor),
        ],
      ),
    );
  }

  List<StarIcon> getStars(double rate)
  {
    List<StarIcon> starsList = new List<StarIcon>();

    int i;
    for (i = 0; i < rate.toInt(); i++)
    {
      starsList.add(new StarIcon(true, widget.starSize, widget.whiteColor));
    }

    double diff = rate - rate.toInt();

    if(diff >= 0.2  && diff <= 0.75) starsList.add(new StarIcon(null, widget.starSize, widget.whiteColor)); // Add half a star if 0.25 > diff > 0.75
    else if(diff > 0.75) starsList.add(new StarIcon(true, widget.starSize, widget.whiteColor)); // Add a full star if > 0.75

    for (int j = diff >= 0.2 ? i+1 : i; j < 5; j++)
    {
      starsList.add(new StarIcon(false, widget.starSize, widget.whiteColor));
    }

    return starsList;
  }
}

class StarIcon extends StatelessWidget
{
  bool state;
  double size;
  bool whiteColor;
  StarIcon(this.state, this.size, this.whiteColor);

  @override
  Widget build(BuildContext context)
  {
    if(state != null) return new Icon(Icons.star, color: state ? whiteColor ? Colors.white : MyColors.starOn : whiteColor ? Colors.white30 : MyColors.starOff, size: size);
    else return new Icon(Icons.star_half, color: whiteColor ? Colors.white : MyColors.starOn, size: size);
  }
}