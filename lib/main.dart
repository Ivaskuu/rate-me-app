import 'package:flutter/material.dart';
import 'ui/ui_main.dart';
import 'misc/mycolors.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget
{
  @override
  Widget build(BuildContext context)
  {
    return new MaterialApp
    (
      title: 'Rate me',
      theme: new ThemeData(primaryColor: MyColors.pinkDark),
      home: new UIMain()
    );
  }
}