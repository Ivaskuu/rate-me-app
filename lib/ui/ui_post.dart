import 'package:flutter/material.dart';
import '../misc/mycolors.dart';
import '../views/star_rate.dart';
import '../ui/rate_review.dart';
import 'dart:async';

class UIPost extends StatefulWidget
{
  final Function loadNextPostFun;
  UIPost(this.loadNextPostFun);

  @override
  _UIPostState createState() => new _UIPostState();
}

class _UIPostState extends State<UIPost>
{
  PageController ctrl = new PageController();
  List<Widget> postAndRateReview = new List();

  @override
  void initState()
  {
    super.initState();
    postAndRateReview.add(new ViewPost(onRate));
  }

  @override
  Widget build(BuildContext context)
  {
    return new Container
    (
      margin: new EdgeInsets.symmetric(horizontal: 12.0),
      child: new PageView.builder
      (
        controller: ctrl,
        scrollDirection: Axis.vertical,
        physics: new NeverScrollableScrollPhysics(),
        itemBuilder: (_, int pos) => postAndRateReview[pos],
        itemCount: postAndRateReview.length,
      )
    );
  }

  void onRate(int starsNum)
  {
    setState(() => postAndRateReview.add(new UIRateReview(starsNum, 'Adrian')));
    ctrl.nextPage(curve: Curves.easeInOut, duration: new Duration(milliseconds: 500));

    new Timer(new Duration(milliseconds: 1000), () => widget.loadNextPostFun());
  }
}

class ViewPost extends StatelessWidget
{
  final Function onRate;
  ViewPost(this.onRate);

  @override
  Widget build(BuildContext context)
  {
    return new Column
    (
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>
      [
        new Container
        (
          margin: new EdgeInsets.only(top: 30.0),
        ),
        new Container
        (
          margin: new EdgeInsets.symmetric(vertical: 10.0),
          child: new Row
          (
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>
            [
              new CircleAvatar(backgroundImage: new AssetImage('res/me.png'), radius: 28.0),
              new Expanded
              (
                child:new ListTile
                (
                  title: new Text('Ivascu Adrian', style: new TextStyle(color: MyColors.textColor, fontSize: 19.0, fontWeight: FontWeight.w400)),
                  subtitle: new Text('4.63', style: new TextStyle(color: MyColors.textColor, fontSize: 27.0, fontWeight: FontWeight.w300)),
                )
              ),
            ],
          ),
        ),
        new Text('Foodie heaven! Didn\'t want to destroy it....', style: new TextStyle(color: MyColors.textColor, fontSize: 18.0, fontWeight: FontWeight.w300), textAlign: TextAlign.left),
        new Container(margin: new EdgeInsets.only(top: 8.0, bottom: 36.0), child: new Image.asset('res/bagels.jpg')),
        new ViewStarRate(execOnRated: onRate),
      ],
    );
  }
}