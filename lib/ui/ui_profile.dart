import 'package:flutter/material.dart';
import '../misc/mycolors.dart';

class UIProfile extends StatefulWidget
{
  @override
  _UIProfileState createState() => new _UIProfileState();
}

class _UIProfileState extends State<UIProfile>
{
  @override
  Widget build(BuildContext context)
  {
    return new Scaffold
    (
      /*appBar: new AppBar
      (
        elevation: 0.0,
        centerTitle: true,
        backgroundColor: MyColors.pinkLight,
        title: new Row
        (
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>
          [
            new Icon(Icons.my_location),
            new Container(margin: new EdgeInsets.only(left: 20.0), child: new Text('Near me')),
          ],
        )
      ),*/
      body: new Container
      (
        decoration: new BoxDecoration
        (
          gradient: MyColors.backgroundGradient
        ),
        child: new ListView
        (
          scrollDirection: Axis.vertical,
          children: <Widget>
          [
            new Column // Image, name and stars
            (
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>
              [
                new Container
                (
                  margin: new EdgeInsets.only(top: 64.0),
                  child: new CircleAvatar(backgroundImage: new AssetImage('res/me.png'), radius: 64.0),
                ),
                new Container
                (
                  margin: new EdgeInsets.only(top: 16.0),
                  child: new Text("Ivascu Adrian", style: Theme.of(context).textTheme.headline.copyWith(fontWeight: FontWeight.w500)),
                ),
                new Container
                (
                  margin: new EdgeInsets.only(top: 8.0),
                  child: new Chip(avatar: new Icon(Icons.star), label: new Text("4.632", style: Theme.of(context).textTheme.title), backgroundColor: Colors.white),
                ),
              ],
            ),
            new Container
            (
              margin: new EdgeInsets.only(left: 12.0, top: 32.0),
              child: new Text('Favourite people', style: Theme.of(context).textTheme.button),
            ),
            new SizedBox.fromSize
            (
              size: new Size.fromHeight(100.0),
              child: new ListView
              (
                scrollDirection: Axis.horizontal,
                children: <Widget>
                [
                  new ViewFavouritePeople(),
                  new ViewFavouritePeople(),
                  new ViewFavouritePeople(),
                  new ViewFavouritePeople(),
                ],
              ),
            ),
            /*new Container
            (
              margin: new EdgeInsets.only(left: 12.0, top: 32.0),
              child: new Text('Posts', style: Theme.of(context).textTheme.button),
            ),*/
            new Container
            (
              margin: new EdgeInsets.all(6.0),
              child: new Material
              (
                elevation: 2.0,
                borderRadius: new BorderRadius.circular(8.0),
                color: Colors.white,
                child: new InkWell
                (
                  onTap: () => null,
                  child: new SizedBox.fromSize
                  (
                    size: new Size.fromHeight(256.0 + 128.0),
                  )
                ),
              ),
            ),
          ],
        )
      )
    );
  }
}

class ViewFavouritePeople extends StatelessWidget
{
  @override
  Widget build(BuildContext context)
  {
    return new Container
    (
      margin: new EdgeInsets.all(6.0),
      child: new Material
      (
        elevation: 2.0,
        borderRadius: new BorderRadius.circular(8.0),
        color: Colors.white,
        child: new InkWell
        (
          onTap: () => null,
          child: new Container
          (
            margin: new EdgeInsets.symmetric(vertical: 8.0, horizontal: 12.0),
            child: new Row
            (
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>
              [
                new CircleAvatar(backgroundImage: new AssetImage('res/me.png'), radius: 32.0),
                new Padding(padding: new EdgeInsets.only(right: 12.0)),
                new Column
                (
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>
                  [
                    new Text('Ivascu Adrian', style: Theme.of(context).textTheme.title),
                    new Text('324 followers'),
                  ],
                ),
              ],
            ),
          ),
        )
      ),
    );
  }
}

