import 'package:flutter/material.dart';
import 'feed.dart';
import 'near.dart';
import 'ui_profile.dart';
import '../misc/mycolors.dart';

Widget body = new UIProfile();

class UIMain extends StatefulWidget
{
  @override
  _UIMainState createState() => new _UIMainState();
}

class _UIMainState extends State<UIMain>
{
  @override
  Widget build(BuildContext context)
  {
    return new Scaffold
    (
      body: body,
      drawer: new ViewDrawer(this)
    );
  }
}

class ViewDrawer extends StatelessWidget
{
  final State uiMainState;
  ViewDrawer(this.uiMainState);

  @override
  Widget build(BuildContext context)
  {
    return new Drawer
    (
      child: new ListView
      (
        children: <Widget>
        [
          /// The user profile
          new InkWell
          (
            onTap: ()
            {
              Navigator.pop(context);
              uiMainState.setState(() => body = new UIProfile());
            },
            child: new Container
            (
              margin: new EdgeInsets.only(top: 52.0, bottom: 32.0),
              child: new Column
              (
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>
                [
                  new CircleAvatar(backgroundImage: new AssetImage('res/me.png'), radius: 40.0),
                  new Container
                  (
                    margin: new EdgeInsets.only(top: 16.0),
                    child: new Text("Ivascu Adrian", style: Theme.of(context).textTheme.headline),
                  ),
                  new Container
                  (
                    margin: new EdgeInsets.only(top: 8.0),
                    child: new Chip(avatar: new Icon(Icons.star), label: new Text("4.632", style: Theme.of(context).textTheme.title)),
                  ),
                ],
              ),
            ),
          ),
          new InkWell
          (
            onTap: ()
            {
              Navigator.pop(context);
              uiMainState.setState(() => body = new UIFeed());
            },
            child: new ListTile
            (
              leading: new Icon(Icons.feedback),
              title: new Text("Feed")
            ),
          ),
          new InkWell
          (
            onTap: ()
            {
              Navigator.pop(context);
              uiMainState.setState(() => body = new UINear());
            },
            child: new ListTile
            (
              leading: new Icon(Icons.my_location),
              title: new Text("Near")
            ),
          ),
          new InkWell
          (
            onTap: () => Navigator.pop(context),
            child: new ListTile
            (
              leading: new Icon(Icons.info),
              title: new Text("Info")
            ),
          ),
          new Container
          (
            margin: new EdgeInsets.only(top: 16.0, left: 16.0),
            child: new Row
            (
              children: <Widget>
              [
                new Text('NOTIFICATIONS', style: Theme.of(context).textTheme.title.copyWith(color: Colors.grey, fontSize: 14.0)),
                new Container
                (
                  margin: new EdgeInsets.only(left: 8.0),
                  child: new CircleAvatar(backgroundColor: Colors.red, radius: 8.0, child: new Text("3", style: new TextStyle(fontSize: 12.0)))
                ),
              ],
            ),
          ),
          new ViewNotificationsFeed()
        ],
      )
    );
  }
}

class ViewNotificationsFeed extends StatelessWidget
{
  @override
  Widget build(BuildContext context)
  {
    return new Column
    (
      children: <Widget>
      [
        new ViewCommentNotification(false),
        new ViewCommentNotification(true),
        new ViewCommentNotification(false),
        new ViewCommentNotification(true),
        new ViewCommentNotification(false),
        new ViewCommentNotification(true),
      ],
    );
  }
}

class ViewCommentNotification extends StatelessWidget
{
  final bool viewed;
  ViewCommentNotification(this.viewed);

  @override
  Widget build(BuildContext context)
  {
    return new InkWell
    (
      onTap: () => Navigator.pop(context),
      child: new ListTile
      (
        isThreeLine: true,
        leading: new CircleAvatar(backgroundImage: new AssetImage('res/me.png'), radius: 40.0),
        title: viewed ? new Text('Adrian commented on your post "This is soo cool!!!"') : new Text('Adrian commented on your post "This is soo cool!!!"', style: new TextStyle(color: Colors.grey)),
        subtitle: new Text('24 minutes ago'),
      ),
    );
  }
}