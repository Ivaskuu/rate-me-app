import 'package:flutter/material.dart';
import 'dart:async';

import '../misc/mycolors.dart';
import '../ui/ui_post.dart';

class UIFeed extends StatefulWidget
{
  List<Widget> posts = new List();

  @override
  _UIFeedState createState() => new _UIFeedState();
}

class _UIFeedState extends State<UIFeed>
{
  PageController pageController;
  bool showRateReview = true;

  @override
  void initState()
  {
    super.initState();
    pageController = new PageController();

    setState(() => widget.posts.add(new UIPost(loadNewPost)));
    loadNewPost();
  }

  @override
  Widget build(BuildContext context)
  {
    return new Scaffold
    (
      body: new SizedBox.expand
      (
        child: new Container
        (
          decoration: new BoxDecoration
          (
            gradient: MyColors.backgroundGradient
          ),
          child: new PageView.builder
          (
            physics: new NeverScrollableScrollPhysics(),
            controller: pageController,
            scrollDirection: Axis.vertical,
            itemBuilder: (_, int pos) => widget.posts[pos],
            itemCount: widget.posts.length,
          ),
        ),
      ),
    );
  }

  void loadNewPost()
  {
    print('load new post');

    setState(() => widget.posts.add(new UIPost(loadNewPost)));
    if(widget.posts.length > 2) scrollPage();
  }

  void scrollPage()
  {
    pageController.nextPage(curve: Curves.fastOutSlowIn, duration: new Duration(milliseconds: 500));
  }
}