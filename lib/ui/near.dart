import 'package:flutter/material.dart';
import '../misc/mycolors.dart';

class UINear extends StatefulWidget
{
  @override
  _UINearState createState() => new _UINearState();
}

class _UINearState extends State<UINear>
{
  @override
  Widget build(BuildContext context)
  {
    return new Scaffold
    (
      appBar: new AppBar
      (
        elevation: 0.0,
        centerTitle: true,
        backgroundColor: MyColors.pinkLight,
        title: new Row
        (
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>
          [
            new Icon(Icons.my_location),
            new Container(margin: new EdgeInsets.only(left: 20.0), child: new Text('Near me')),
          ],
        )
      ),
      body: new SizedBox.expand
      (
        child: new Container
        (
          decoration: new BoxDecoration
          (
            gradient: MyColors.backgroundGradient
          ),
          child: new GridView.count
          (
            crossAxisCount: 2,
            children: <Widget>
            [
              new ViewProfileNear(),
              new ViewProfileNear(),
              new ViewProfileNear(),
              new ViewProfileNear(),
              new ViewProfileNear(),
              new ViewProfileNear(),
              new ViewProfileNear(),
            ],
          )
        ),
      ),
    );
  }
}

class ViewProfileNear extends StatelessWidget
{
  @override
  Widget build(BuildContext context)
  {
    return new Card
    (
      child: new InkWell
      (
        onTap: () => print('hi'),
        child: new Container
        (
          child: new Column
          (
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>
            [
              new CircleAvatar(backgroundImage: new AssetImage('res/me.png'), radius: 48.0),
              new Container
              (
                margin: new EdgeInsets.only(top: 8.0),
                child: new Text('Adrian', style: new TextStyle(fontSize: 18.0, fontWeight: FontWeight.w500), textAlign: TextAlign.center),
              ),
              new Row
              (
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>
                [
                  new Icon(Icons.star),
                  new Container(margin: new EdgeInsets.only(left: 4.0), child: new Text("4.6", style: Theme.of(context).textTheme.title))
                ],
              ),
            ],
          )
        ),
      ),
    );
  }
}