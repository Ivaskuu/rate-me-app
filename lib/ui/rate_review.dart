import 'package:flutter/material.dart';
import '../views/star_rate.dart';
import 'dart:async';

class UIRateReview extends StatelessWidget
{
  int starsNum;
  String userName;
  UIRateReview(this.starsNum, this.userName);

  @override
  Widget build(BuildContext context)
  {
    return new Column
    (
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>
      [
        new Container
        (
          margin: new EdgeInsets.only(bottom: 32.0),
          child: new CircleAvatar(backgroundImage: new AssetImage('res/me.png'), radius: 80.0),
        ),
        new Text('Rated', style: new TextStyle(color: Colors.white, fontSize: 42.0, fontWeight: FontWeight.w300), textAlign: TextAlign.center),
        new Container
        (
          margin: new EdgeInsets.only(bottom: 64.0),
          child: new Row
          (
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>
            [
              new Text(userName, style: new TextStyle(color: Colors.white, fontSize: 42.0, fontWeight: FontWeight.w500)),
              new Text(starsNum > 1 ? ' $starsNum stars' : ' $starsNum star', style: new TextStyle(color: Colors.white, fontSize: 42.0, fontWeight: FontWeight.w300)),
            ],
          ),
        ),
        new ViewStarRate(rate: starsNum + 0.0, whiteColor: true)
      ],
    );
  }
}