import 'package:flutter/material.dart';

class MyColors
{
  /// Background
  static Color pinkLight = new Color.fromRGBO(253, 205, 191, 1.0);
  static Color pinkDark = new Color.fromRGBO(255, 183, 178, 1.0);

  /// Stars
  static Color starOn = new Color.fromRGBO(249, 90, 255, 1.0);
  static Color starOff = new Color.fromRGBO(223, 174, 196, 1.0);

  /// Text
  static Color textColor = new Color.fromRGBO(46, 11, 61, 1.0);

  static LinearGradient backgroundGradient = new LinearGradient
  (
    colors: [ pinkLight, pinkDark ],
    begin: new Alignment(0.0, 0.0),
    end: new Alignment(1.0, 1.0)
  );

  static TextStyle textStyle = new TextStyle(color: textColor);
}